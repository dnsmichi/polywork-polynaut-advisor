# Polywork Polynaut Advisor

Hi, I'm Michael aka @dnsmichi and I am Polynaut Advisor for [Polywork](https://www.polywork.com/). Connect with me using [your VIP access code](http://polywork.com/invite/dnsmichi) 🦊 

Public project for feedback, screenshots, ideas, thoughts collection.

> **Note**
>
> This project is for my (@dnsmichi) personal feedback collection. **It is not an official Polywork feedback channel**, and as such there is no guarantee that your feedback is heard. Tag [@Polywork](https://twitter.com/Polywork) [@dnsmichi](https://twitter.com/dnsmichi) on Twitter for ideas on this project. Feel free to fork and use for your own purpose.

## Overview

- [Issues](https://gitlab.com/dnsmichi/polywork-polynaut-advisor/-/issues) including screenshots with notes, using [GitLab's design management](https://docs.gitlab.com/ee/user/project/issues/design_management.html) feature. 
  - [Feedback board](https://gitlab.com/dnsmichi/polywork-polynaut-advisor/-/boards/3625040)

## Workflow

1. Create a new issue using the [feedback template](https://gitlab.com/dnsmichi/polywork-polynaut-advisor/-/issues/new?issuable_template=feedback)
1. Fill in the details, upload screenshots, add comments
1. Verify the labels ~"feedback::new"

### Update feedback

Navigate into the [Feedback board](https://gitlab.com/dnsmichi/polywork-polynaut-advisor/-/boards/3625040) and drag&drop the issue into the ~"feedback::acknowledged" list.

Alternatively, use keyboard shortcuts and quick actions: `r`, `/label ~"feedback::acknowledged"` and `cmd+enter` when the issue is opened. 

## Insights

I (@dnsmichi) am using Polywork at https://www.polywork.com/dnsmichi and regularly collect feedback and ideas shared with the Polywork team. This project helps to keep my feedback organized, and publicly accessible for both, Polywork team and myself for writing blog posts with insights.

