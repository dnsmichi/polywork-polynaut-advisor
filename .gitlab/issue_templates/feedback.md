<!-- This issue template can be used a great starting point for feature ideas, feedback, bug reports, etc. -->

### Problem to solve ❔ 

<!-- What is the user problem you are trying to solve with this issue? -->

### Proposal 💡 

<!-- Use this section to explain the feature and how it will work. It can be helpful to add technical details, design proposals, and links to related epics or issues. -->

### Intended users 🤗  

Who will use this feature? If known, include any of the following: types of users (e.g. DevRel), personas, roles. 

* Niclas, DevOps Engineer
* Priyanka, General Manager
* Valerie, Content Editor 
* Rachel, Meetup organizer
* Dan, Podcast Host
* Fatima, Developer Evangelist 
* Brendan, Keynote Speaker
* Abubakar, Mentor
* Melissa, Marketing Leader
* John, DevRel Manager

### User experience goal 🌈

What is the single user experience workflow this problem addresses?
<!-- https://about.gitlab.com/handbook/engineering/ux/ux-research-training/user-story-mapping/ -->

### Further details 🚀

Include use cases, benefits, goals, or any other details that will help understand the problem better. 

#### Device information 🏗️

These are @dnsmichi's devices in a template to easier select them.

- [ ] Macbook Pro, macOS Monterey
- [ ] iPhone 11 Pro
  - iOS 15.x
- [ ] iPad Pro
  - iPad OS 15.x



<!-- Quick actions applied below. -->

/label ~"feedback::new"

<!-- Quick action workflows: https://about.gitlab.com/blog/2021/02/18/improve-your-gitlab-productivity-with-these-10-tips/ -->


